{- 
 -
 -                4
 - pi = -------------------
 -                1^2
 -       1 + --------------
 -                   3^2
 -            2 + ---------
 -                     5^2
 -                 2 + ----
 -                      ...
 -}
pi1 :: Int -> Double
pi1 n = 4 / (1 + (pi1' n 1))
    where pi1' 1 numerator = 2
          pi1' n numerator = (numerator ^ 2) / (2 + (pi1' (n-1) (numerator + 2)))
{-
 -                  1^2
 - pi = 3 + -------------------
 -                   3^2
 -           6 + -------------
 -                     5^2
 -                6 + ------
 -                     ...
 -}

pi2 :: Int -> Double
pi2 n = 3 + pi2' n 1
    where pi2' 1 numerator = 6
          pi2' n numerator = (numerator ^ 2) / (6 + pi2' (n-1) (numerator + 2))
 
{-
 -                 4
 - pi = ------------------------
 -                   1^2
 -       1 + ------------------
 -                    2^2
 -            3 + -----------
 -                     3^2
 -                 5 + ---
 -                     ...
 -}
pi3 :: Int -> Double
pi3 n = 4 / (1 + (pi3' n 1 3))
    where pi3' 1 numerator denominator = numerator
          pi3' n numerator denominator = ((numerator ^ 2) /
              (denominator + (pi3' (n-1) (numerator + 1) (denominator + 2))))
 
{-       4     4     4     4
 - pi = --- - --- + --- - --- + ...
 -       1     3     5     7
 -}
pi4 :: Int -> Double
pi4 n = if n == 0 then 4.0 else (-1)**(fromIntegral n)*(4/(2*fromIntegral n+1.0)) + pi4 (n-1)

{-             4         4         4         4
 - pi = 3 + ------- - ------- + ------- - -------- + ...
 -           2*3*4     4*5*6     6*7*8     8*9*10
 -}
pi5 :: Int -> Double
pi5 n = let n' = (fromIntegral n) in
 if n == 0 then 3.0 else
        	     pi5 (n-1) + (-1)**(n'-1)*(4 / ((2*n'+2)*(2*n')*(2*n'+1)))
 