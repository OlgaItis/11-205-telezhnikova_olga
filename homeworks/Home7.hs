-- арифметические прогрессии в простых числах длины 3 с расстоянием k
-- например, take 2 (arith 2) = [[5,11,17], [7,13,19]]
nat = 1 : map (+1) nat

isPrime 1 = False
isPrime 2 = True
isPrime n | even n = False
          | otherwise = null $ filter (\i -> n`mod`i == 0) $ takeWhile (\i -> i*i <=n) [3,5..]

primes = filter isPrime nat

getKTail :: Int -> [Int] -> [Int]
getKTail k xs | k == 0 = xs
			   | otherwise = getKTail (k-1) $ tail xs

progres k = let primes' = getKTail k primes
                primes'' = getKTail (2*k) primes
             in zip3' primes primes' primes''
                
zip3' (a:as) (b:bs) (c:cs) = [a,b,c] : zip3' as bs cs
zip3' _ _ _ = []


isProgres (x:y:z:xs) | x - y == y - z = True
				     | otherwise = False

arith :: Int -> [[Int]]
arith k = filter isProgres $ progres k

-- положение ферзей на шахматной доске nxn, при котором они не бьют друг друга
-- список номеров горизонталей, на которых находятся ферзи
-- например, Board [1,2,3,4,5,6,8,7] -- это такое расположение
--  +--------+
-- 8|      ♕ |
-- 7|       ♕|
-- 6|     ♕  |
-- 5|    ♕   |
-- 4|   ♕    |
-- 3|  ♕     |
-- 2| ♕      |
-- 1|♕       |
-- -+--------+
--  |abcdefgh|

newtype Board = Board { unBoard :: [Int] } deriving (Eq,Show)

queens :: Int -> [Board]
queens n = map (\x -> Board { unBoard = x}) $ test n
  where test :: Int -> [[Int]]
        test n =  filter (\x -> isSolution x ) $ perms [1..n]

        -- Generate all permutations of the list
        perms :: [Int] -> [[Int]]
        perms [] = [[]]
        perms xs = [ x:ps | x <- xs , ps <- perms ( delete x xs ) ]
          where delete :: Int -> [Int] -> [Int]
                delete _ []     = []
                delete x (y:ys) = if x == y then ys else y : delete x ys

        isSolution :: [Int] -> Bool
        isSolution [] = True
        isSolution (y:ys) = (check y ys 1) && isSolution ys
          where check a [] n = True
                check a (x:xs) n = (abs(a-x) /= n) && check a xs (n+1)

-- Белые начинают и дают мат в два хода
-- 
-- Белые: Кd8 Kh8 Сf7 Крf6
-- Черные: Крf8 Кe7 Kg7
-- 
-- (написать перебор всех вариантов полуходов длины три,
-- вернуть список последовательностей полуходов, ведущих
-- к решению; до этого определить необходимые типы)
-- См. https://en.wikipedia.org/wiki/Chess_notation

-- Объявите тип Move так, как вам нужно
type Move = Int

solutions :: [[Move]]
solutions = undefined

