

{- Нахождение максимума в списке -}
myMaximum :: [Int] -> Int
myMaximum [] = 0
myMaximum (x:xs) = helper (x:xs) 0
    where helper [] a = a
          helper (x:xs) max |  x> max = helper xs x
                            | otherwise = helper xs max

-- | Нахождение максимум и его индекс 
-- >>> myMaxIndex [1,5,2,3,4]
-- (1,5)
myMaxIndex :: [Integer] -> (Int,Integer)
myMaxIndex l = 
   let pmax :: [Integer] -> Int -> (Int, Integer)
       pmax [x] xi = (xi, x)
       pmax (x:xs) xi | x > t = (xi, x)
                      | otherwise = (ti, t)
                      where (ti, t) = pmax xs (xi + 1)
   in pmax l 0
-- | Количество элементов, равных максимальному
-- >>> maxCount [1,5,3,10,3,10,5]
-- 2
maxCount :: [Integer] -> Int
maxCount (x:xs) = helper (x:xs) 0 0
    where helper [] a b = b
          helper (x:xs) max count |  x> max = helper xs x 1
                                  | x == max = helper xs x count+1
                                  | otherwise = helper xs max count
    

-- | Количество элементов между минимальным и максимальным
-- >>> countBetween [-1,3,100,3]
-- 2
--
-- >>> countBetween [100,3,-1,3]
-- -2
--
-- >>> countBetween [-1,100]
-- 1
--
-- >>> countBetween [1]
-- 0
countBetween :: [Integer] -> Int
countBetween l = idx where
  maxIndex = fst (myMaxIndex l)
  myMinIndex :: [Integer] -> Int -> (Int, Integer)
  myMinIndex [x] xi = (xi, x)
  myMinIndex (x:xs) xi | x < t = (xi, x)
                       | otherwise = (ti, t)
                       where (ti, t) = myMinIndex xs (xi + 1)
  minIndex = fst (myMinIndex l 0)
  idx = maxIndex - minIndex 
