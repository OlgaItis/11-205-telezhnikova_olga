
-- | tribo_n = tribo_{n-1} + tribo_{n-2} + tribo_{n-3}
--   tribo_0 = 1
--   tribo_1 = 1
--   tribo_2 = 1
--
-- >>> tribo 0
-- 1
--
-- >>> tribo 1
-- 1
--
-- >>> tribo 2
-- 1
--
-- >>> tribo 3
-- 3
--
-- >>> odd (tribo 100)
-- True
tribo :: Int -> Integer
tribo n = helper n 1 1 1
	where helper 0 a b c = a
              helper 1 a b c = b
              helper 2 a b c = c
              helper n a b c = helper (n-1) b c (a+b+c)

-- Тип Цвет, который может быть Белым, Черным, Красным, Зелёным, Синим, либо Смесь из трёх чисел (0-255)
-- операции Сложение :: Цвет -> Цвет -> Цвет
-- (просто складываются интенсивности, если получилось >255, то ставится 255)
-- ПолучитьКрасныйКанал, ПолучитьЗелёныйКанал, ПолучитьСинийКанал :: Цвет -> Int

data Color = White
             |Black
             |Red
             |Blue
             |Green
             |Mix (Int, Int, Int)
             deriving (Show)

colorSum :: Color -> Color -> Color
colorSum c1 c2 = helper (getRGB(c1)) (getRGB(c2))
    where helper (a1,b1,c1) (a2,b2,c2) | a1+a2 > 255 = helper (0,b1,c1) (255,b2,c2)
                                       | b1+b2 > 255 = helper (a1,0,c1) (a2,255,c2)
                                       | c1+c2 > 255 = helper (a1,b1,0) (a2,b2,255)
                                       | otherwise = Mix (a1+a2,b1+b2,c1+c2)

getRGB :: Color -> (Int, Int,Int)

getRGB White = (255,255,255)
getRGB Black = (0,0,0)
getRGB Red = (255,0,0)
getRGB Blue = (0,0,255)
getRGB Green = (0,255,0)
getRGB (Mix(a,b,c)) = (a,b,c)
